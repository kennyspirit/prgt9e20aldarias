/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpannel2s;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-mar-2014
 */
public class Main {

  boolean packFrame = false;

  /**
   * Construct and show the application.
   */
  public Main() {
    MainFrame frame = new MainFrame();
    // Validate frames that have preset sizes 
    // Pack frames that have useful preferred size info, e.g. from their layout 
    if (packFrame) {
      frame.pack();
    } else {
      frame.validate();
    }

    // Center the window 
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = frame.getSize();
    if (frameSize.height > screenSize.height) {
      frameSize.height = screenSize.height;
    }
    if (frameSize.width > screenSize.width) {
      frameSize.width = screenSize.width;
    }
    frame.setLocation((screenSize.width - frameSize.width) / 2,
            (screenSize.height - frameSize.height) / 2);
    frame.setVisible(true);
  }

  /**
   * Application entry point.
   *
   * @param args String[]
   */
  public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        try {
          UIManager.setLookAndFeel(UIManager.
                  getSystemLookAndFeelClassName());
        } catch (Exception exception) {
          exception.printStackTrace();
        }

        new Main();
      }
    });
  }
}
